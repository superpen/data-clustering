import numpy as np
import math
import matplotlib.pyplot as plt

def calc_distance(point1, point2):
    distance = math.sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2)
    return distance

def pos_lookup(source_clusters, value1, value2):
    #function for checking in which cluster lays the value
    pos_1 = "n"
    pos_2 = "n"
    i = 0
    for cluster in source_clusters:
        if value1 in cluster:
            pos_1 = i
        if value2 in cluster:
            pos_2 = i
        if isinstance(pos_1,int) and isinstance(pos_2,int):
            return pos_1, pos_2
        i += 1
        
def indices2data(data_points, clusters):
    result = []
    valuable_clusters = filter(None, clusters)
    for cluster in valuable_clusters:
        cluster_data = np.take(data_points, cluster, axis = 0)
        result.append(cluster_data)
    return result

def clustering(data_points, distance_matrix, number_of_clusters):
    data_size = len(data_points)
    
    clusters = []
    for i in range(data_size):
        clusters.append([i])
        
    # recieving indicies of all non zero distances    
    indicies_nonzero_distance = np.nonzero(distance_matrix)
    # recieving all meaningful distances and flatten them in D1 array
    flat_array_distances = distance_matrix[indicies_nonzero_distance]
    # recieveing unique, sorted D1 array of distances
    sorted_distances = np.sort(np.unique(flat_array_distances))
        
    #going through every unique distance
    for distance in sorted_distances:
        #recieving all points with the choosen distance in between - two tuples
        rows, columns = np.where(distance_matrix==distance)
        for i in range(len(rows)):
            #finding the position of the first candidate point in the clusters array
            #we search by value, cause it can be part of another cluster already)
            position1, position2 = pos_lookup(clusters, rows[i], columns[i])
            if position1 == position2:
                pass
            else:
                #merging points in a cluster
                clusters[position1] += clusters[position2]
                #replacing merged point with an empty value
                clusters.pop(position2)
                #checking if we reached the right amount of clusters
            if len(clusters) == number_of_clusters:
                return indices2data(data_points, clusters)


data = np.loadtxt('valid_data.txt',  delimiter=';')
data_size = len(data)
distance_matrix = np.zeros((data_size,data_size))

for index1 in range(data_size):
    for index2 in range(index1, data_size):
        distance = calc_distance(data[index1], data[index2])
        distance_matrix.itemset((index1,index2), distance)

clusters_res = clustering(data, distance_matrix, 5)

for cluster in clusters_res:
    plt.scatter(cluster[:,0], cluster[:,1], cmap="rainbow")
    
plt.show()
