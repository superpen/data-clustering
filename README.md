# Clustering #

This repository was created to display my solution for the task from the course DataMining at Univesity Bielefeld (https://ekvv.uni-bielefeld.de/sinfo/publ/modul/26787742)

## Task ##

An example from research. To explore a room, the robot BIRON records it using a 3D camera. The data represents the room as many single points. An approach for the human to find relevant areas and objects within the room is to assume that they differ from the bottom plane and thus have a height above zero. If we look at the height of the filtered points and cluster them, we get a mapping of these areas.

You can find the data set points.dat. It includes 5 columns of which only the first 4 are interesting for us:

 - X-axis
 - Y-axis
 - Z-axis (height)
 - Validity (-1 = valid / 0 = invalid)

First filter out all the invalid points and all points below the bottom plane (Z<0). To reduce the data set even more you should only use every 20th entry in your further work. About 2000 points should remain (**in this repository you will find only preprocess data, only ~ 2000 filtered data points**). 

Now cluster the remaining points. The height is not relevant anymore and can be assumed to be 0 to get points in a 2-dimensional room. Use the basic scheme of agglomerative clustering as presented in the lecture to cluster the data. Use the number of clusters as the termination condition. Try 5, 4 and 3 clusters.

Compare the resulting clusters that you get, if you use...

a) Single Linkage Clustering (SLC).

 1. Initialization: Regard data points as 1-element cluster C 1 , ... , C n .
 2. Find pair of clusters C i , C j with ( i ⋆ , j ⋆ ) = arg min d( C i , C j ) i<j Set d ̃ = d( C i ⋆ , C j ⋆ )
 3. Replace C i ⋆ ← C i ⋆ ∪ C j ⋆
 4. If j ⋆ < n : replace C j ⋆ ← C n
 5. Set n ← n − 1
 6. If termination condition A( C 1 , ... , C n ) = false: goto 2 termination conditions: number of clusters: n ≤ n goal n error: ∑ Var( C i ) > E goal i=1 distance: d ̃ > d max

run file slc.py

b) Complete Linkage Clustering (CLC).

run file clc.py

### Prerequisites ###

python 3, numpy, math, matplotlib, sklearn
