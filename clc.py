import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering

data = np.loadtxt('valid_data.txt',  delimiter=';')
cluster = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='complete')
cluster.fit_predict(data)
plt.scatter(data[:,0],data[:,1], c=cluster.labels_, cmap='rainbow')
